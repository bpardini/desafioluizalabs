package luizalabs.bruno.com.desafioluizalabs.data.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Bruno on 08/04/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CycleCity {

    @JsonProperty("name")
    private String cityName;
    @JsonProperty("coord")
    private CityCoord cityCoord;
    @JsonProperty("main")
    private CityTemp cityTemp;
    @JsonProperty("weather")
    private List<CityWeather> cityWeather;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public CityCoord getCityCoord() {
        return cityCoord;
    }

    public void setCityCoord(CityCoord cityCoord) {
        this.cityCoord = cityCoord;
    }

    public CityTemp getCityTemp() {
        return cityTemp;
    }

    public void setCityTemp(CityTemp cityTemp) {
        this.cityTemp = cityTemp;
    }

    public List<CityWeather> getCityWeather() {
        return cityWeather;
    }

    public void setCityWeather(List<CityWeather> cityWeather) {
        this.cityWeather = cityWeather;
    }
}
