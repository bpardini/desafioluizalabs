package luizalabs.bruno.com.desafioluizalabs.ui.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import luizalabs.bruno.com.desafioluizalabs.R;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.permission.PermissionContract;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.permission.PermissionPresenter;

/**
 * Created by Bruno on 08/04/2017.
 */

public class FragmentPermission extends BaseFragment implements PermissionContract.View {

    public static final String TAG = FragmentPermission.class.getName();
    private String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    public static final int REQUEST_LOCATION = 1;
    private PermissionContract.Presenter presenter;
    private Menu mMenu;

    public static FragmentPermission newInstance(){
        return new FragmentPermission();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_permission, container, false);

        setUnBinder(ButterKnife.bind(this, rootView));

        setup();

        return rootView;
    }

    @OnClick(R.id.bt_give_permission)
    public void buttonGivePermissionClick(){
        presenter.requestLocationPermission();
    }

    @Override
    void setup() {
        presenter = new PermissionPresenter(getActivity(), this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_LOCATION:
                if(grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Log.i(TAG, "Location permission OK");
                    mMenu.findItem(R.id.item_celsius).setEnabled(true);
                    mMenu.findItem(R.id.item_fahrenheit).setEnabled(true);

                    getActivity().getSupportFragmentManager().popBackStack(TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    FragmentListCities fragmentListCities = FragmentListCities.newInstance();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, fragmentListCities)
                            .commit();
                }
                else{
                    Log.i(TAG, "Location permission denied.");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void setLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.
            requestPermissions(PERMISSIONS_LOCATION, FragmentPermission.REQUEST_LOCATION);
        }
        else{
            requestPermissions(PERMISSIONS_LOCATION, FragmentPermission.REQUEST_LOCATION);
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        mMenu = menu;
        super.onCreateOptionsMenu(menu, menuInflater);
    }
}
