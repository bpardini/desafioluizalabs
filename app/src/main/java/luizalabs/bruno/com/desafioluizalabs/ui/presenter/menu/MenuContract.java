package luizalabs.bruno.com.desafioluizalabs.ui.presenter.menu;

/**
 * Created by Bruno on 11/04/2017.
 */

public interface MenuContract {

    interface Presenter{
        void setIconViewType();
        void setIconTemperatureType();
        void setCelsiusVisible();
        void setFahrenheitVisible();
        void setListVisible();
        void setMapVisible();
    }

}
