package luizalabs.bruno.com.desafioluizalabs.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Bruno on 08/04/2017.
 */

public class CityCoord {

    @JsonProperty("lat")
    private double latitude;
    @JsonProperty("lon")
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
