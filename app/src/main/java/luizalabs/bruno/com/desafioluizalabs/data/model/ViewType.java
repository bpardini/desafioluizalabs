package luizalabs.bruno.com.desafioluizalabs.data.model;

/**
 * Created by BPardini on 07/04/2017.
 */

public enum ViewType {

    LIST("L"),
    MAP("M");

    private final String mType;

    ViewType(String type){
        mType = type;
    }

    public String getType(){
        return mType;
    }

}
