package luizalabs.bruno.com.desafioluizalabs.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;

import luizalabs.bruno.com.desafioluizalabs.R;
import luizalabs.bruno.com.desafioluizalabs.data.model.WeatherCity;

/**
 * Created by Bruno on 09/04/2017.
 */

public class LocationUtils {

    public static void showAlertDialogProviderNotEnabled(final Activity activity){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AlertDialogStyle);
        builder.setTitle(R.string.title_activate_location);
        builder.setMessage(R.string.message_activate_location);
        builder.setPositiveButton(R.string.yes, (dialog, id) -> {
            activity.startActivity(new Intent(Settings.ACTION_SETTINGS));
            dialog.dismiss();
        });
        builder.setNegativeButton(R.string.no, (dialog, id) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static ArrayList<WeatherCity> modifyWeatherListAccordingToMenuItem(ArrayList<WeatherCity> mWeatherCities, int menuId){
        ArrayList<WeatherCity> weatherCityArrayList = new ArrayList<>();
        if(menuId == R.id.item_celsius) {
            for (WeatherCity weatherCity : mWeatherCities) {
                weatherCity.setTemperature((int) Math.round(TemperatureUtils.convertFahrenheitToCelsius(weatherCity.getTemperature())));
                weatherCity.setMaxTemperature((int) Math.round(TemperatureUtils.convertFahrenheitToCelsius(weatherCity.getMaxTemperature())));
                weatherCity.setMinTemperature((int) Math.round(TemperatureUtils.convertFahrenheitToCelsius(weatherCity.getMinTemperature())));
                weatherCityArrayList.add(weatherCity);
            }
        }
        else if(menuId == R.id.item_fahrenheit){
            for(WeatherCity weatherCity : mWeatherCities){
                weatherCity.setTemperature((int)Math.round(TemperatureUtils.convertCelsiusToFahrenheit(weatherCity.getTemperature())));
                weatherCity.setMaxTemperature((int)Math.round(TemperatureUtils.convertCelsiusToFahrenheit(weatherCity.getMaxTemperature())));
                weatherCity.setMinTemperature((int)Math.round(TemperatureUtils.convertCelsiusToFahrenheit(weatherCity.getMinTemperature())));
                weatherCityArrayList.add(weatherCity);
            }
        }
        else{
            return null;
        }

        return weatherCityArrayList;
    }

}
