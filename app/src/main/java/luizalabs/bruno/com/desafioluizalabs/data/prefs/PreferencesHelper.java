package luizalabs.bruno.com.desafioluizalabs.data.prefs;

/**
 * Created by BPardini on 07/04/2017.
 */

public interface PreferencesHelper {

    String getCurrentViewType();

    void setCurrentViewType(String viewType);

    String getCurrentTemperatureType();

    void setCurrentTemperatureType(String temperatureType);

}
