package luizalabs.bruno.com.desafioluizalabs.factory;

import com.google.android.gms.location.LocationRequest;

/**
 * Created by Bruno on 09/04/2017.
 */

public class LocationRequestFactory {

    public static final int second = 1000 * 10;
    public static final int minute = 1000 * 60;
    public static final int kilometer = 1000;

    private static final long FASTEST_INTERVAL = 10 * second;
    private static final long INTERVAL = 7 * minute;
    private static final int PRIORITY = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
    private static final long SMALLEST_DISPLACEMENT = 10 * kilometer;

    public static LocationRequest getDefaultLocationRequest() {
        return LocationRequest
                .create()
                .setFastestInterval(FASTEST_INTERVAL)
                .setInterval(INTERVAL)
                .setPriority(PRIORITY)
                .setSmallestDisplacement(SMALLEST_DISPLACEMENT);
    }

}
