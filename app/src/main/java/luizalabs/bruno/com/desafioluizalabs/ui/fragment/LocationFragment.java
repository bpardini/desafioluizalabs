package luizalabs.bruno.com.desafioluizalabs.ui.fragment;

import android.app.Activity;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import butterknife.Unbinder;
import luizalabs.bruno.com.desafioluizalabs.R;
import luizalabs.bruno.com.desafioluizalabs.factory.LocationRequestFactory;
import luizalabs.bruno.com.desafioluizalabs.utils.CommonUtils;
import luizalabs.bruno.com.desafioluizalabs.utils.GooglePlayUtils;

/**
 * Created by BPardini on 10/04/2017.
 */

public abstract class LocationFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    public static final String TAG = LocationFragment.class.getName();

    private Unbinder mUnbinder;
    private Activity mActivity;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    protected Location mLastLocation;
    private boolean isGoogleApiClientStarting;

    private static final int REQUEST_GPS_RESOLUTION = 10000;

    public void setActivity(Activity activity){
        mActivity = activity;

        if (mGoogleApiClient == null && !isGoogleApiClientStarting) {
            isGoogleApiClientStarting = true;
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            mLocationRequest = LocationRequestFactory.getDefaultLocationRequest();
        }
        else{
            locationUpdate();
        }
    }

    public void setUnBinder(Unbinder unBinder) {
        mUnbinder = unBinder;
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    abstract void setup();

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        isGoogleApiClientStarting = false;
        if(bundle != null) {
            try {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            } catch (SecurityException e) {
                CommonUtils.showSnackBar(getActivity(), (CoordinatorLayout)mActivity.findViewById(R.id.coordinator_main), getString(R.string.gps_not_allowed_error));
            }
        }

        locationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        isGoogleApiClientStarting = false;
        Log.i(TAG, "Connection suspended");
        CommonUtils.showSnackBar(getActivity(), (CoordinatorLayout)mActivity.findViewById(R.id.coordinator_main), getString(R.string.connection_suspended));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        isGoogleApiClientStarting = false;
        if (connectionResult.getErrorMessage() != null && !connectionResult.getErrorMessage().isEmpty()) {
            CommonUtils.showSnackBar(getActivity(), (CoordinatorLayout)mActivity.findViewById(R.id.coordinator_main), connectionResult.getErrorMessage());
        } else {
            CommonUtils.showSnackBar(getActivity(), (CoordinatorLayout)mActivity.findViewById(R.id.coordinator_main), GooglePlayUtils.getStatusMessage(getActivity()));
        }
    }

    void locationUpdate(){
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> pendingResult = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        pendingResult.setResultCallback((locationSettingsResult) ->{
            switch (locationSettingsResult.getStatus().getStatusCode()){
                case LocationSettingsStatusCodes.SUCCESS:
                    successAccessUpdate();
                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    try {
                        locationSettingsResult.getStatus()
                                .startResolutionForResult(getActivity(), REQUEST_GPS_RESOLUTION);
                    } catch (IntentSender.SendIntentException e) {
                        CommonUtils.showSnackBar(getActivity(), (CoordinatorLayout)mActivity.findViewById(R.id.coordinator_main), getString(R.string.gps_not_allowed_error));
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    CommonUtils.showSnackBar(getActivity(), (CoordinatorLayout)mActivity.findViewById(R.id.coordinator_main), getString(R.string.gps_not_allowed_error));
                    break;
            }
        });
    }

    void successAccessUpdate() {
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException e) {
            CommonUtils.showSnackBar(getActivity(), (CoordinatorLayout)mActivity.findViewById(R.id.coordinator_main), getString(R.string.gps_not_allowed_error));
        }
    }

}
