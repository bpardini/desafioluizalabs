package luizalabs.bruno.com.desafioluizalabs.ui.presenter.permission;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import luizalabs.bruno.com.desafioluizalabs.utils.LocationUtils;

/**
 * Created by Bruno on 08/04/2017.
 */

public class PermissionPresenter implements PermissionContract.Presenter {

    private Activity mActivity;
    private PermissionContract.View mPermissionView;
    private LocationManager locationManager;

    public PermissionPresenter(Activity activity, PermissionContract.View permissionView){
        mActivity = activity;
        mPermissionView = permissionView;
    }

    @Override
    public void requestLocationPermission(){
        locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);

        if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            if (!checkIfPermissionsGranted()) {
                mPermissionView.setLocationPermission();
            } else {
                //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, new LocationProvider(this, locationManager, escolas));
            }
        }
        else{
            LocationUtils.showAlertDialogProviderNotEnabled(mActivity);
        }
    }

    public boolean checkIfPermissionsGranted(){
        return ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
}
