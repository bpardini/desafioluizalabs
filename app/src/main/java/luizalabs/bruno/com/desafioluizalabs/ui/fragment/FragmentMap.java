package luizalabs.bruno.com.desafioluizalabs.ui.fragment;

import android.app.ProgressDialog;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import luizalabs.bruno.com.desafioluizalabs.R;
import luizalabs.bruno.com.desafioluizalabs.data.model.CityCoord;
import luizalabs.bruno.com.desafioluizalabs.data.model.WeatherCity;
import luizalabs.bruno.com.desafioluizalabs.data.prefs.AppPreferencesHelper;
import luizalabs.bruno.com.desafioluizalabs.data.prefs.PreferencesHelper;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.location.LocationContract;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.location.LocationPresenter;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.map.MapContract;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.map.MapPresenter;
import luizalabs.bruno.com.desafioluizalabs.utils.AppConstants;
import luizalabs.bruno.com.desafioluizalabs.utils.CommonUtils;
import luizalabs.bruno.com.desafioluizalabs.utils.LocationUtils;
import luizalabs.bruno.com.desafioluizalabs.utils.NetworkUtils;

/**
 * Created by BPardini on 06/04/2017.
 */

public class FragmentMap extends LocationFragment implements OnMapReadyCallback, LocationContract.View {

    @BindView(R.id.coordinator_map)
    CoordinatorLayout coordinatorLayout;

    public static final String TAG = FragmentMap.class.getName();

    private ProgressDialog mProgress;
    private SupportMapFragment mSupportMapFragment;
    private MapContract.Presenter presenterMap;
    private LocationContract.Presenter presenterLocation;
    private PreferencesHelper preferencesHelper;
    private GoogleMap mGoogleMap;
    private Bundle outState;
    private ArrayList<WeatherCity> mWeatherCities;

    public static FragmentMap newInstance(){
        return new FragmentMap();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        mSupportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        setUnBinder(ButterKnife.bind(this, rootView));
        setActivity(getActivity());

        setup();

        return rootView;
    }

    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        outState = savedInstanceState;
    }

    @Override
    void setup() {
        preferencesHelper = new AppPreferencesHelper(getActivity());
        presenterMap = new MapPresenter(getActivity());
        presenterLocation = new LocationPresenter(this, preferencesHelper);

        if(mSupportMapFragment == null){
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mSupportMapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.map, mSupportMapFragment).commit();
        }

        mSupportMapFragment.getMapAsync(this);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.item_celsius:
                presenterMap.mapReady(mGoogleMap, LocationUtils.modifyWeatherListAccordingToMenuItem(mWeatherCities, R.id.item_celsius));
                return true;
            case R.id.item_fahrenheit:
                presenterMap.mapReady(mGoogleMap, LocationUtils.modifyWeatherListAccordingToMenuItem(mWeatherCities, R.id.item_fahrenheit));
                return true;
            default:
                return onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(WeatherCity.BUNDLE_LIST, mWeatherCities);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void showLoading() {
        hideLoading();
        mProgress = CommonUtils.showLoadingDialog(getActivity(), "");
    }

    @Override
    public void hideLoading() {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.cancel();
        }
    }

    @Override
    public void onSuccessLocationsLoad(ArrayList<WeatherCity> weatherCities) {
        if(weatherCities.size() > 0) {
            mWeatherCities = weatherCities;
            presenterMap.mapReady(mGoogleMap, weatherCities);
        }
    }

    @Override
    public void onErrorLocationsLoad(int code) {
        switch (code) {
            case AppConstants.STATUS_CODE_ERROR:
                Toast.makeText(getActivity(), getString(R.string.error_load_cities), Toast.LENGTH_SHORT).show();
                break;
            case AppConstants.STATUS_NOT_AVAILABLE:
                Toast.makeText(getActivity(), getString(R.string.cities_not_available), Toast.LENGTH_SHORT).show();
                break;
            case AppConstants.STATUS_CODE_NOT_FOUND:
                Toast.makeText(getActivity(), getString(R.string.cities_not_found), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(getActivity(), getString(R.string.error_load_cities), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        CityCoord cityCoord = new CityCoord();
        cityCoord.setLatitude(location.getLatitude());
        cityCoord.setLongitude(location.getLongitude());

        if(outState != null){
            mWeatherCities =  outState.getParcelableArrayList(WeatherCity.BUNDLE_LIST);
            presenterMap.mapReady(mGoogleMap, mWeatherCities);
        }
        else if(mWeatherCities == null){
            if(NetworkUtils.isNetworkAvailable(getActivity())) {
                presenterLocation.getWeatherCities(cityCoord);
            }
            else{
                CommonUtils.showSnackbarWithAction(getActivity(), coordinatorLayout, "Ok", getString(R.string.network_unavailable));
            }
        }
    }
}
