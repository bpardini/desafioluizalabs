package luizalabs.bruno.com.desafioluizalabs.data.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Bruno on 08/04/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CityList {

    @JsonProperty("list")
    private List<CycleCity> cycleCities;

    public List<CycleCity> getCycleCities() {
        return cycleCities;
    }

    public void setCycleCities(List<CycleCity> cycleCities) {
        this.cycleCities = cycleCities;
    }
}
