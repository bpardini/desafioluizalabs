package luizalabs.bruno.com.desafioluizalabs.ui.adapter;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import luizalabs.bruno.com.desafioluizalabs.R;
import luizalabs.bruno.com.desafioluizalabs.data.model.WeatherCity;
import luizalabs.bruno.com.desafioluizalabs.utils.TemperatureUtils;

/**
 * Created by BPardini on 06/04/2017.
 */

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<WeatherCity> mListWeatherCity;

    public CitiesAdapter(Activity activity, ArrayList<WeatherCity> listWeatherCity){
        mActivity = activity;
        mListWeatherCity = listWeatherCity;
    }

    public void updateList(ArrayList<WeatherCity> weatherCityArrayList){
        mListWeatherCity = weatherCityArrayList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        WeatherCity weatherCity = mListWeatherCity.get(position);
        if(weatherCity != null){
            holder.bind(weatherCity);
        }
    }

    @Override
    public int getItemCount() {
        return mListWeatherCity.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_city_name)
        TextView tvCityName;

        @BindView(R.id.img_weather)
        ImageView imgWeather;

        @BindView(R.id.tv_weather)
        TextView tvWeather;

        @BindView(R.id.tv_temperature)
        TextView tvTemperature;

        @BindView(R.id.tv_min_temperature)
        TextView tvMinTemperature;

        @BindView(R.id.tv_max_temperature)
        TextView tvMaxTemperature;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(WeatherCity weatherCity){
            tvCityName.setText(weatherCity.getCityName());
            tvWeather.setText(weatherCity.getWeather());
            Glide.with(mActivity)
                    .load(weatherCity.getWeatherImagePath())
                    .into(imgWeather);
            tvTemperature.setText(mActivity.getString(R.string.temperature, weatherCity.getTemperature()));
            tvTemperature.setTextColor(ContextCompat.getColor(mActivity, TemperatureUtils.getColorAccordingToTemperature(weatherCity.getTemperature())));
            tvMinTemperature.setText(mActivity.getString(R.string.min_temperature, weatherCity.getMinTemperature()));
            tvMaxTemperature.setText(mActivity.getString(R.string.max_temperature, weatherCity.getMaxTemperature()));
        }
    }

}
