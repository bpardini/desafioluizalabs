package luizalabs.bruno.com.desafioluizalabs.utils;

import luizalabs.bruno.com.desafioluizalabs.R;

/**
 * Created by BPardini on 05/04/2017.
 */

public class TemperatureUtils {

    public static double convertKelvinToCelsius(double kelvin){
        return kelvin - 273.15;
    }

    public static double convertFahrenheitToCelsius(double fahrenheit){
        return (fahrenheit-32)/1.8;
    }

    public static double convertCelsiusToFahrenheit(double celsius){
        return (celsius * 1.8) + 32;
    }

    public static int getColorAccordingToTemperature(int temperature){
        if(temperature > 26){
            return R.color.very_hot;
        }
        else if(temperature > 20){
            return R.color.hot;
        }
        else if(temperature > 14){
            return R.color.warm;
        }
        else if(temperature > 0){
            return R.color.cold;
        }
        else{
            return R.color.freezing;
        }
    }
}
