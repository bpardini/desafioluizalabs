package luizalabs.bruno.com.desafioluizalabs.data.model;

/**
 * Created by BPardini on 07/04/2017.
 */

public enum TemperatureType {

    CELSIUS("C"),
    FAHRENHEIT("F");

    private final String mTemperatureType;

    TemperatureType(String temperatureType){
        mTemperatureType = temperatureType;
    }

    public String getType(){
        return mTemperatureType;
    }

}
