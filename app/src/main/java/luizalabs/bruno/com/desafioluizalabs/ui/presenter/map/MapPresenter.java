package luizalabs.bruno.com.desafioluizalabs.ui.presenter.map;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import luizalabs.bruno.com.desafioluizalabs.data.model.WeatherCity;
import luizalabs.bruno.com.desafioluizalabs.factory.CustomInfoWindow;

/**
 * Created by BPardini on 07/04/2017.
 */

public class MapPresenter implements MapContract.Presenter {

    public static final String TAG = MapPresenter.class.getName();

    private Activity mActivity;
    private GoogleMap mMap;
    private Marker currentShown;
    private CustomInfoWindow mCustomInfoWindow;

    public MapPresenter(Activity activity){
        mActivity = activity;
        mCustomInfoWindow = new CustomInfoWindow(mActivity);
    }

    @Override
    public void mapReady(GoogleMap googleMap, ArrayList<WeatherCity> weatherCities) {
        Log.d(TAG, "mapReady");
        mMap = googleMap;
        mMap.clear();
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if(ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setTiltGesturesEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.setInfoWindowAdapter(mCustomInfoWindow);
        mMap.setOnMarkerClickListener(marker -> {
            if (marker.equals(currentShown)) {
                marker.hideInfoWindow();
                currentShown = null;
            } else {
                marker.showInfoWindow();
                currentShown = marker;
                int zoom = (int)mMap.getCameraPosition().zoom;
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(marker.getPosition().latitude + (double)90/Math.pow(2, zoom), marker.getPosition().longitude), zoom);
                mMap.animateCamera(cameraUpdate);
            }
            return true;
        });

        LatLng latLng = new LatLng(-23.686957, -46.454031);

        createMarkers(weatherCities);
    }

    private void createMarkers(ArrayList<WeatherCity> weatherCities){
        List<Marker> listMarkers = new ArrayList<>();

        for(WeatherCity weatherCity : weatherCities){
            LatLng latLng = new LatLng(weatherCity.getLatitude(), weatherCity.getLongitude());

            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(weatherCity.getCityName())
                    .snippet(weatherCity.getTemperature() + "º " + weatherCity.getWeather()));

            Glide.with(mActivity).load(weatherCity.getWeatherImagePath())
                    .asBitmap().fitCenter().into(new SimpleTarget<Bitmap>(140,140) {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(resource);
                    marker.setIcon(icon);
                }
            });
            listMarkers.add(marker);
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(Marker marker : listMarkers){
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        int padding = 60;
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);

        mMap.animateCamera(cameraUpdate);
    }
}
