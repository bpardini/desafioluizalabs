package luizalabs.bruno.com.desafioluizalabs.ui.presenter.menu;

import android.view.Menu;

import luizalabs.bruno.com.desafioluizalabs.R;
import luizalabs.bruno.com.desafioluizalabs.data.model.TemperatureType;
import luizalabs.bruno.com.desafioluizalabs.data.model.ViewType;
import luizalabs.bruno.com.desafioluizalabs.data.prefs.PreferencesHelper;

/**
 * Created by Bruno on 11/04/2017.
 */

public class MenuPresenter implements MenuContract.Presenter {

    private PreferencesHelper mPreferencesHelper;
    private Menu mMenu;

    public MenuPresenter(PreferencesHelper preferencesHelper, Menu menu){
        mPreferencesHelper = preferencesHelper;
        mMenu = menu;
    }

    @Override
    public void setIconViewType() {
        if(mPreferencesHelper.getCurrentViewType().equals(ViewType.LIST.getType())){
            mMenu.findItem(R.id.item_map).setVisible(true);
            mMenu.findItem(R.id.item_list).setVisible(false);
        }
        else{
            mMenu.findItem(R.id.item_map).setVisible(false);
            mMenu.findItem(R.id.item_list).setVisible(true);
        }
    }

    @Override
    public void setIconTemperatureType() {
        if(mPreferencesHelper.getCurrentTemperatureType().equals(TemperatureType.CELSIUS.getType())){
            mMenu.findItem(R.id.item_celsius).setVisible(false);
            mMenu.findItem(R.id.item_fahrenheit).setVisible(true);
        }
        else{
            mMenu.findItem(R.id.item_celsius).setVisible(true);
            mMenu.findItem(R.id.item_fahrenheit).setVisible(false);
        }
    }

    @Override
    public void setCelsiusVisible() {
        mMenu.findItem(R.id.item_celsius).setVisible(true);
        mMenu.findItem(R.id.item_fahrenheit).setVisible(false);
    }

    @Override
    public void setFahrenheitVisible() {
        mMenu.findItem(R.id.item_celsius).setVisible(false);
        mMenu.findItem(R.id.item_fahrenheit).setVisible(true);
    }

    @Override
    public void setListVisible() {
        mMenu.findItem(R.id.item_map).setVisible(false);
        mMenu.findItem(R.id.item_list).setVisible(true);
    }

    @Override
    public void setMapVisible() {
        mMenu.findItem(R.id.item_map).setVisible(true);
        mMenu.findItem(R.id.item_list).setVisible(false);
    }


}
