package luizalabs.bruno.com.desafioluizalabs.utils;

/**
 * Created by BPardini on 05/04/2017.
 */

public class AppConstants {

    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public static final String URL_FIND = "find";
    public static final String URL_CITY = "city";
    public static final String ICONS_URL = "http://openweathermap.org/img/w/";

    public static final int TIMEOUT = 60;
    public static final int NUMBER_OF_THREADS = 20;

    public static final int STATUS_CODE_SUCCESS = 200;
    public static final int STATUS_CODE_SUCCESS_NO_BODY = 204;
    public static final int STATUS_CODE_ERROR = 500;
    public static final int STATUS_CODE_NOT_FOUND = 404;
    public static final int STATUS_NOT_AVAILABLE = 503;

}
