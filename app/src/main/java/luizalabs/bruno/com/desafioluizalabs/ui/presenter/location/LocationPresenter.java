package luizalabs.bruno.com.desafioluizalabs.ui.presenter.location;

import android.location.Location;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import luizalabs.bruno.com.desafioluizalabs.BuildConfig;
import luizalabs.bruno.com.desafioluizalabs.data.model.CityCoord;
import luizalabs.bruno.com.desafioluizalabs.data.model.CityList;
import luizalabs.bruno.com.desafioluizalabs.data.model.CycleCity;
import luizalabs.bruno.com.desafioluizalabs.data.model.TemperatureType;
import luizalabs.bruno.com.desafioluizalabs.data.model.WeatherCity;
import luizalabs.bruno.com.desafioluizalabs.data.prefs.PreferencesHelper;
import luizalabs.bruno.com.desafioluizalabs.data.remote.RestApi;
import luizalabs.bruno.com.desafioluizalabs.utils.AppConstants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Bruno on 08/04/2017.
 */

public class LocationPresenter implements LocationContract.Presenter {

    public static final String TAG = LocationPresenter.class.getName();

    private RestApi mRestApi;
    private LocationContract.View mLocationView;
    private PreferencesHelper mPreferencesHelper;

    public LocationPresenter(LocationContract.View locationView, PreferencesHelper preferencesHelper){
        mLocationView = locationView;
        mPreferencesHelper = preferencesHelper;
        mRestApi = RestApi.Builder.build();
    }

    @Override
    public void getWeatherCities(final CityCoord cityCoord) {
        mLocationView.showLoading();
        Map<String, String> map = new HashMap<>();
        map.put("lat", String.valueOf(cityCoord.getLatitude()));
        map.put("lon", String.valueOf(cityCoord.getLongitude()));
        if(mPreferencesHelper.getCurrentTemperatureType() == null || TemperatureType.CELSIUS.getType().equals(mPreferencesHelper.getCurrentTemperatureType())){
            map.put("units", "metric");
        }
        else{
            map.put("units", "imperial");
        }
        map.put("lang", "pt");
        map.put("cnt", "50");
        map.put("appid", BuildConfig.OPEN_WEATHER_MAP_API_KEY);

        Call<CityList> weatherCities = mRestApi.getWeatherCities(map);
        weatherCities.enqueue(new Callback<CityList>() {
            @Override
            public void onResponse(Call<CityList> call, Response<CityList> response) {
                mLocationView.hideLoading();
                if(response.code() == AppConstants.STATUS_CODE_SUCCESS){
                    List<CityCoord> coords = new ArrayList<>();
                    ArrayList<WeatherCity> listWeatherCity = new ArrayList<>();

                    float[] dist = new float[response.body().getCycleCities().size()];

                    for(CycleCity cycleCity : response.body().getCycleCities()){
                        Location.distanceBetween(cityCoord.getLatitude(), cityCoord.getLongitude(), cycleCity.getCityCoord().getLatitude(), cycleCity.getCityCoord().getLongitude(), dist);

                        if(!coords.contains(cycleCity.getCityCoord()) && dist[0]/1000 < 50){
                            Log.d("Cidade", cycleCity.getCityName());
                            Log.d("Distance from origin", dist[0]/1000+"");
                            CityCoord cityCoord = new CityCoord();
                            cityCoord.setLatitude(cycleCity.getCityCoord().getLatitude());
                            cityCoord.setLongitude(cycleCity.getCityCoord().getLongitude());
                            coords.add(cityCoord);

                            WeatherCity weatherCity = new WeatherCity();
                            weatherCity.setCityName(cycleCity.getCityName());
                            weatherCity.setTemperature(Math.round(cycleCity.getCityTemp().getTemperature()));
                            weatherCity.setMaxTemperature(Math.round(cycleCity.getCityTemp().getMaxTemperature()));
                            weatherCity.setMinTemperature(Math.round(cycleCity.getCityTemp().getMinTemperature()));
                            weatherCity.setWeather(cycleCity.getCityWeather().get(0).getWeatherDescription());
                            weatherCity.setWeatherImagePath(AppConstants.ICONS_URL + cycleCity.getCityWeather().get(0).getIcon() + ".png");
                            weatherCity.setLatitude(cycleCity.getCityCoord().getLatitude());
                            weatherCity.setLongitude(cycleCity.getCityCoord().getLongitude());

                            listWeatherCity.add(weatherCity);
                        }
                    }

                    mLocationView.onSuccessLocationsLoad(listWeatherCity);
                }
                else{
                    mLocationView.onErrorLocationsLoad(response.code());
                }
            }

            @Override
            public void onFailure(Call<CityList> call, Throwable t) {
                mLocationView.hideLoading();
                Log.e(TAG, t.getMessage());
                mLocationView.onErrorLocationsLoad(AppConstants.STATUS_CODE_ERROR);
            }
        });
    }
}
