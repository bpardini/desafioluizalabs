package luizalabs.bruno.com.desafioluizalabs.ui.presenter.map;

import com.google.android.gms.maps.GoogleMap;

import java.util.ArrayList;

import luizalabs.bruno.com.desafioluizalabs.data.model.WeatherCity;

/**
 * Created by BPardini on 07/04/2017.
 */

public interface MapContract {

    interface Presenter{
        void mapReady(GoogleMap googleMap, ArrayList<WeatherCity> weatherCities);
    }

}
