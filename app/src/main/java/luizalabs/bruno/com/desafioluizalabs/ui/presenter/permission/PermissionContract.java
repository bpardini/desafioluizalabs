package luizalabs.bruno.com.desafioluizalabs.ui.presenter.permission;

/**
 * Created by Bruno on 08/04/2017.
 */

public interface PermissionContract {

    interface View{
        void setLocationPermission();
    }

    interface Presenter{
        void requestLocationPermission();
        boolean checkIfPermissionsGranted();
    }

}
