package luizalabs.bruno.com.desafioluizalabs.ui.fragment;

import android.app.ProgressDialog;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import luizalabs.bruno.com.desafioluizalabs.R;
import luizalabs.bruno.com.desafioluizalabs.data.model.CityCoord;
import luizalabs.bruno.com.desafioluizalabs.data.model.WeatherCity;
import luizalabs.bruno.com.desafioluizalabs.data.prefs.AppPreferencesHelper;
import luizalabs.bruno.com.desafioluizalabs.data.prefs.PreferencesHelper;
import luizalabs.bruno.com.desafioluizalabs.ui.adapter.CitiesAdapter;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.location.LocationContract;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.location.LocationPresenter;
import luizalabs.bruno.com.desafioluizalabs.utils.AppConstants;
import luizalabs.bruno.com.desafioluizalabs.utils.CommonUtils;
import luizalabs.bruno.com.desafioluizalabs.utils.LocationUtils;
import luizalabs.bruno.com.desafioluizalabs.utils.NetworkUtils;

/**
 * Created by BPardini on 06/04/2017.
 */

public class FragmentListCities extends LocationFragment implements LocationContract.View{

    public static final String TAG = FragmentListCities.class.getName();

    @BindView(R.id.coordinator_list_cities)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.recycler_cities)
    RecyclerView recyclerCities;

    private ProgressDialog mProgress;

    private LocationContract.Presenter presenter;
    private ArrayList<WeatherCity> mWeatherCities;
    private CitiesAdapter citiesAdapter;
    private PreferencesHelper preferencesHelper;
    private Bundle outState;

    public static FragmentListCities newInstance() {
        return new FragmentListCities();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_list_cities, container, false);

        setUnBinder(ButterKnife.bind(this, rootView));
        setActivity(getActivity());

        return rootView;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setup();

        outState = savedInstanceState;

        if(savedInstanceState != null){
            mWeatherCities = savedInstanceState.getParcelableArrayList(WeatherCity.BUNDLE_LIST);
            citiesAdapter = new CitiesAdapter(getActivity(), mWeatherCities);
            recyclerCities.setAdapter(citiesAdapter);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(WeatherCity.BUNDLE_LIST, mWeatherCities);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.item_celsius:
                citiesAdapter.updateList(LocationUtils.modifyWeatherListAccordingToMenuItem(mWeatherCities, R.id.item_celsius));
                return true;
            case R.id.item_fahrenheit:
                citiesAdapter.updateList(LocationUtils.modifyWeatherListAccordingToMenuItem(mWeatherCities, R.id.item_fahrenheit));
                return true;
            default:
                return onOptionsItemSelected(item);
        }
    }

    @Override
    void setup() {
        mWeatherCities = new ArrayList<>();
        preferencesHelper = new AppPreferencesHelper(getActivity());
        presenter = new LocationPresenter(this, preferencesHelper);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerCities.setLayoutManager(manager);
    }

    @Override
    public void showLoading() {
        hideLoading();
        mProgress = CommonUtils.showLoadingDialog(getActivity(), "");
    }

    @Override
    public void hideLoading() {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.cancel();
        }
    }

    @Override
    public void onSuccessLocationsLoad(ArrayList<WeatherCity> weatherCities) {
        if(weatherCities.size() > 0 && recyclerCities != null) {
            mWeatherCities = weatherCities;
            citiesAdapter = new CitiesAdapter(getActivity(), weatherCities);
            recyclerCities.setAdapter(citiesAdapter);
        }
    }

    @Override
    public void onErrorLocationsLoad(int code) {
        switch (code) {
            case AppConstants.STATUS_CODE_ERROR:
                CommonUtils.showSnackBar(getActivity(), coordinatorLayout, getString(R.string.error_load_cities));
                break;
            case AppConstants.STATUS_NOT_AVAILABLE:
                CommonUtils.showSnackBar(getActivity(), coordinatorLayout, getString(R.string.cities_not_available));
                break;
            case AppConstants.STATUS_CODE_NOT_FOUND:
                CommonUtils.showSnackBar(getActivity(), coordinatorLayout, getString(R.string.cities_not_found));
                break;
            default:
                CommonUtils.showSnackBar(getActivity(), coordinatorLayout, getString(R.string.error_load_cities));
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(getActivity() != null && outState == null && mWeatherCities.size() == 0){
            if(NetworkUtils.isNetworkAvailable(getActivity())) {
                mLastLocation = location;
                CityCoord cityCoord = new CityCoord();
                cityCoord.setLatitude(location.getLatitude());
                cityCoord.setLongitude(location.getLongitude());
                presenter.getWeatherCities(cityCoord);
            }
            else{
                CommonUtils.showSnackbarWithAction(getActivity(), coordinatorLayout, "Ok", getString(R.string.network_unavailable));
            }
        }
    }
}
