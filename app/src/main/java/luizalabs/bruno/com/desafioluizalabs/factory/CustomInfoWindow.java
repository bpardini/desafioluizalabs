package luizalabs.bruno.com.desafioluizalabs.factory;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import luizalabs.bruno.com.desafioluizalabs.R;

/**
 * Created by BPardini on 10/04/2017.
 */

public class CustomInfoWindow implements GoogleMap.InfoWindowAdapter {

    private final View myContentsView;

    public CustomInfoWindow(Activity activity){
        myContentsView = activity.getLayoutInflater().inflate(R.layout.custom_infowindow, null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        TextView tvTitle = (TextView)myContentsView.findViewById(R.id.tv_title);
        TextView tvSnippet = (TextView)myContentsView.findViewById(R.id.tv_snippet);
        tvTitle.setText(marker.getTitle());
        tvSnippet.setText(marker.getSnippet());

        return myContentsView;
    }
}
