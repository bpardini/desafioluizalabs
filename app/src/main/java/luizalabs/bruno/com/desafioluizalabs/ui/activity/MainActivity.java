package luizalabs.bruno.com.desafioluizalabs.ui.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import luizalabs.bruno.com.desafioluizalabs.R;
import luizalabs.bruno.com.desafioluizalabs.data.model.TemperatureType;
import luizalabs.bruno.com.desafioluizalabs.data.model.ViewType;
import luizalabs.bruno.com.desafioluizalabs.data.prefs.AppPreferencesHelper;
import luizalabs.bruno.com.desafioluizalabs.data.prefs.PreferencesHelper;
import luizalabs.bruno.com.desafioluizalabs.ui.fragment.FragmentListCities;
import luizalabs.bruno.com.desafioluizalabs.ui.fragment.FragmentMap;
import luizalabs.bruno.com.desafioluizalabs.ui.fragment.FragmentPermission;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.menu.MenuContract;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.menu.MenuPresenter;
import luizalabs.bruno.com.desafioluizalabs.utils.CommonUtils;

public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getName();

    @BindView(R.id.coordinator_main)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private MenuContract.Presenter presenterMenu;
    private PreferencesHelper preferencesHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUnBinder(ButterKnife.bind(this));

        setup();

        if(preferencesHelper.getCurrentViewType() == null){
            preferencesHelper.setCurrentViewType(ViewType.LIST.getType());
        }

        if(preferencesHelper.getCurrentTemperatureType() == null){
            preferencesHelper.setCurrentTemperatureType(TemperatureType.CELSIUS.getType());
        }

        if (findViewById(R.id.fragment_container) != null && getSupportFragmentManager().getBackStackEntryCount() == 0 && savedInstanceState == null) {
            if (checkIfPermissionGranted()) {
                FragmentListCities fragmentListCities = FragmentListCities.newInstance();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, fragmentListCities)
                        .commit();
            }
            else{
                FragmentPermission fragmentPermission = FragmentPermission.newInstance();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, fragmentPermission).commit();
            }
        }
    }

    @Override
    void setup() {
        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);

        preferencesHelper = new AppPreferencesHelper(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        presenterMenu = new MenuPresenter(preferencesHelper, menu);

        getMenuInflater().inflate(R.menu.menu_main, menu);

        if(preferencesHelper.getCurrentViewType() != null){
            presenterMenu.setIconViewType();
        }

        if(preferencesHelper.getCurrentTemperatureType() != null){
            presenterMenu.setIconTemperatureType();
        }

        if(!checkIfPermissionGranted()){
            menu.findItem(R.id.item_fahrenheit).setEnabled(false);
            menu.findItem(R.id.item_map).setEnabled(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.item_celsius:
                changeTemperature(R.id.item_celsius);
                CommonUtils.showSnackBar(this, coordinatorLayout, getString(R.string.show_celsius));
                return false;
            case R.id.item_fahrenheit:
                changeTemperature(R.id.item_fahrenheit);
                CommonUtils.showSnackBar(this, coordinatorLayout, getString(R.string.show_fahrenheit));
                return false;
            case R.id.item_map:
                changeView(R.id.item_map);
                return true;
            case R.id.item_list:
                changeView(R.id.item_list);
                return true;
            default:
                return onOptionsItemSelected(item);
        }
    }

    private boolean checkIfPermissionGranted(){
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void changeTemperature(int item){
        if(item == R.id.item_celsius){
            preferencesHelper.setCurrentTemperatureType(TemperatureType.CELSIUS.getType());
            presenterMenu.setFahrenheitVisible();
        }
        else{
            preferencesHelper.setCurrentTemperatureType(TemperatureType.FAHRENHEIT.getType());
            presenterMenu.setCelsiusVisible();
        }
    }

    private void changeView(int item){
        Fragment fragment;
        String tag;

        if(item == R.id.item_map){
            preferencesHelper.setCurrentViewType(ViewType.MAP.getType());
            fragment = FragmentMap.newInstance();
            presenterMenu.setListVisible();
            tag = FragmentMap.TAG;
        }
        else{
            preferencesHelper.setCurrentViewType(ViewType.LIST.getType());
            fragment = FragmentListCities.newInstance();
            presenterMenu.setMapVisible();
            tag = FragmentListCities.TAG;
        }

        if(getSupportFragmentManager().getBackStackEntryCount() > 0){
            getSupportFragmentManager().popBackStack();
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }

    @Override
    public void onBackPressed(){
        if(getSupportFragmentManager().getBackStackEntryCount() > 0){
            String fragmentTag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
            if(fragmentTag.equals(FragmentListCities.TAG)){
                finish();
            }
            else{
                preferencesHelper.setCurrentViewType(ViewType.LIST.getType());
                presenterMenu.setMapVisible();
                getSupportFragmentManager().popBackStack();
            }
        }
        else{
            finish();
        }
    }
}
