package luizalabs.bruno.com.desafioluizalabs.ui.presenter.location;

import java.util.ArrayList;

import luizalabs.bruno.com.desafioluizalabs.data.model.CityCoord;
import luizalabs.bruno.com.desafioluizalabs.data.model.WeatherCity;

/**
 * Created by Bruno on 08/04/2017.
 */

public interface LocationContract {

    interface View{
        void showLoading();
        void hideLoading();
        void onSuccessLocationsLoad(ArrayList<WeatherCity> weatherCities);
        void onErrorLocationsLoad(int code);
    }

    interface Presenter{
        void getWeatherCities(CityCoord cityCoord);
    }

}
