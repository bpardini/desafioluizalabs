package luizalabs.bruno.com.desafioluizalabs.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by BPardini on 07/04/2017.
 */

public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_VIEW_TYPE = "PREF_KEY_VIEW_TYPE";
    private static final String PREF_KEY_TEMPERATURE_TYPE = "PREF_KEY_TEMPERATURE_TYPE";

    private final SharedPreferences mPrefs;

    public AppPreferencesHelper(Context context){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getCurrentViewType(){
        return mPrefs.getString(PREF_KEY_VIEW_TYPE, null);
    }

    public void setCurrentViewType(String viewType){
        mPrefs.edit().putString(PREF_KEY_VIEW_TYPE, viewType).apply();
    }

    public String getCurrentTemperatureType(){
        return mPrefs.getString(PREF_KEY_TEMPERATURE_TYPE, null);
    }

    public void setCurrentTemperatureType(String temperatureType){
        mPrefs.edit().putString(PREF_KEY_TEMPERATURE_TYPE, temperatureType).apply();
    }
}
