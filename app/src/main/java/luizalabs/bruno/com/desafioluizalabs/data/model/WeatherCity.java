package luizalabs.bruno.com.desafioluizalabs.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by BPardini on 06/04/2017.
 */

public class WeatherCity implements Parcelable {

    public static final String BUNDLE_LIST = "weather_cities";

    private String cityName;
    private String weather;
    private String weatherImagePath;
    private int temperature;
    private int minTemperature;
    private int maxTemperature;
    private double latitude;
    private double longitude;

    public WeatherCity(){}

    private WeatherCity(Parcel in) {
        cityName = in.readString();
        weather = in.readString();
        weatherImagePath = in.readString();
        temperature = in.readInt();
        minTemperature = in.readInt();
        maxTemperature = in.readInt();
    }

    public static final Creator<WeatherCity> CREATOR = new Creator<WeatherCity>() {
        @Override
        public WeatherCity createFromParcel(Parcel in) {
            return new WeatherCity(in);
        }

        @Override
        public WeatherCity[] newArray(int size) {
            return new WeatherCity[size];
        }
    };

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getWeatherImagePath() {
        return weatherImagePath;
    }

    public void setWeatherImagePath(String weatherImagePath) {
        this.weatherImagePath = weatherImagePath;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(int minTemperature) {
        this.minTemperature = minTemperature;
    }

    public int getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(int maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cityName);
        dest.writeString(weather);
        dest.writeString(weatherImagePath);
        dest.writeInt(temperature);
        dest.writeInt(minTemperature);
        dest.writeInt(maxTemperature);
    }
}
