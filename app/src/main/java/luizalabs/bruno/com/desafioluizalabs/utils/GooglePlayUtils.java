package luizalabs.bruno.com.desafioluizalabs.utils;

import android.content.Context;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import luizalabs.bruno.com.desafioluizalabs.R;

/**
 * Created by BPardini on 10/04/2017.
 */

public class GooglePlayUtils {

    public static String getStatusMessage(Context context){
        final int playServicesStatus = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        switch (playServicesStatus) {
            case ConnectionResult.API_UNAVAILABLE:
                return context.getResources().getString(R.string.play_services_api_unavailable);
            case ConnectionResult.NETWORK_ERROR:
                return context.getResources().getString(R.string.play_services_network_error);
            case ConnectionResult.RESTRICTED_PROFILE:
                return context.getResources().getString(R.string.play_services_restricted_profile);
            case ConnectionResult.SERVICE_MISSING:
                return context.getResources().getString(R.string.play_services_missing);
            case ConnectionResult.SIGN_IN_REQUIRED:
                return context.getResources().getString(R.string.play_services_sign_in_required);
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                return context.getResources().getString(R.string.play_services_update_required);
            case ConnectionResult.SUCCESS:
                return "";
        }
        return context.getResources().getString(R.string.general_error);
    }

}
