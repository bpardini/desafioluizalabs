package luizalabs.bruno.com.desafioluizalabs.presenter;

import android.app.Activity;

import com.google.android.gms.maps.GoogleMap;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;

import luizalabs.bruno.com.desafioluizalabs.data.model.WeatherCity;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.map.MapPresenter;

/**
 * Created by Bruno on 13/04/2017.
 */

public class MapPresenterTest {

    @Mock
    private GoogleMap mMap;

    @Mock
    private Activity mActivity;

    private MapPresenter mMapPresenter;
    private ArrayList<WeatherCity> weatherCities;

    private void populateCitiesList(){
        WeatherCity weatherCity = new WeatherCity();
        weatherCity.setCityName("Maua");
        weatherCity.setLatitude(-23.686957);
        weatherCity.setLongitude(-46.454031);
        weatherCity.setTemperature(32);
        weatherCity.setMinTemperature(25);
        weatherCity.setMaxTemperature(33);

        weatherCities = new ArrayList<>();
        weatherCities.add(weatherCity);
    }

    @Before
    public void setupMapPresenter(){
        populateCitiesList();

        mMapPresenter = new MapPresenter(mActivity);
    }

    @Test
    public void mapReady(){
        mMapPresenter.mapReady(mMap, weatherCities);
    }

}
