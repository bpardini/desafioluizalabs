package luizalabs.bruno.com.desafioluizalabs.core;

import org.junit.Test;

import luizalabs.bruno.com.desafioluizalabs.R;
import luizalabs.bruno.com.desafioluizalabs.utils.TemperatureUtils;

import static org.junit.Assert.assertEquals;

/**
 * Created by Bruno on 13/04/2017.
 */
public class TemperatureColorConvertionTest {

    @Test
    public void getColorFromTemperature(){
        //Testing the very hot temperature
        int temperature = 28;
        int expectedColor = R.color.very_hot;
        assertEquals(expectedColor, TemperatureUtils.getColorAccordingToTemperature(temperature));

        //Testing the hot temperature
        temperature = 23;
        expectedColor = R.color.hot;
        assertEquals(expectedColor, TemperatureUtils.getColorAccordingToTemperature(temperature));

        //Testing the weam temperature
        temperature = 19;
        expectedColor = R.color.warm;
        assertEquals(expectedColor, TemperatureUtils.getColorAccordingToTemperature(temperature));

        //Testing the cold temperature
        temperature = 5;
        expectedColor = R.color.cold;
        assertEquals(expectedColor, TemperatureUtils.getColorAccordingToTemperature(temperature));

        //Testing the freezing temperature
        temperature = -10;
        expectedColor = R.color.freezing;
        assertEquals(expectedColor, TemperatureUtils.getColorAccordingToTemperature(temperature));
    }

}
