package luizalabs.bruno.com.desafioluizalabs.core;

import org.junit.Test;

import luizalabs.bruno.com.desafioluizalabs.utils.TemperatureUtils;

import static org.junit.Assert.assertEquals;

/**
 * Created by BPardini on 05/04/2017.
 */

public class TemperatureConversionTest {

    @Test
    public void convertKelvinToCelsius(){
        double expectedCelsius = 26.85;
        assertEquals(expectedCelsius, TemperatureUtils.convertKelvinToCelsius(300), 0.01);

        expectedCelsius = -3.15;
        assertEquals(expectedCelsius, TemperatureUtils.convertKelvinToCelsius(270), 0.01);
    }

    @Test
    public void convertCelsiusToFahrenheit(){
        double expectedFahrenheit = 77;
        assertEquals(expectedFahrenheit, TemperatureUtils.convertCelsiusToFahrenheit(25), 0.01);

        expectedFahrenheit = -23.8;
        assertEquals(expectedFahrenheit, TemperatureUtils.convertCelsiusToFahrenheit(-31), 0.01);
    }

    @Test
    public void convertFahrenheitToCelsius(){
        double expectedCelsius = 15.55;
        assertEquals(expectedCelsius, TemperatureUtils.convertFahrenheitToCelsius(60), 0.01);

        expectedCelsius = -25;
        assertEquals(expectedCelsius, TemperatureUtils.convertFahrenheitToCelsius(-13), 0.01);
    }

}
