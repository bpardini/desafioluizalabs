package luizalabs.bruno.com.desafioluizalabs.presenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import luizalabs.bruno.com.desafioluizalabs.data.model.CityCoord;
import luizalabs.bruno.com.desafioluizalabs.data.prefs.PreferencesHelper;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.location.LocationContract;
import luizalabs.bruno.com.desafioluizalabs.ui.presenter.location.LocationPresenter;

/**
 * Created by Bruno on 13/04/2017.
 */

public class LocationPresenterTest {

    @Mock
    private LocationContract.View mLocationView;

    @Mock
    private PreferencesHelper mPreferencesHelper;

    private LocationPresenter mLocationPresenter;

    @Before
    public void setupLocationPresenter(){
        MockitoAnnotations.initMocks(this);

        mLocationPresenter = new LocationPresenter(mLocationView, mPreferencesHelper);
    }

    @Test
    public void getWeatherCities(){
        CityCoord cityCoord = new CityCoord();
        cityCoord.setLatitude(-23.686957);
        cityCoord.setLongitude(-46.454031);

        mLocationPresenter.getWeatherCities(cityCoord);
    }
}
